﻿using Lobby.Settings.Views;
using UnityEngine;

namespace Base.View
{
    public class RootView : MonoBehaviour
    {
        #region Fields

        [SerializeField] private SettingsView settingsView;

        #endregion

        #region Properties

        public SettingsView SettingsView => settingsView;

        #endregion
    }
}