﻿using Zenject;

namespace Gameplay.Zenject
{
    public class GameplayInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<GameplaySystemsExecutor>()
                .AsSingle()
                .NonLazy();
        }
    }
}
